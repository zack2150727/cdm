{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "MarginCallBase",
    "package" : "model::external::cdm",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "instructionType",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Identifies the enumeration values to specify the call notification type, direction, specific action type."
      } ],
      "type" : "model::external::cdm::MarginCallInstructionType"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "party",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents the parties to the margin call. The cardinality is optional to address the case where both parties of the event are specified and a third party if applicable."
      } ],
      "type" : "model::external::cdm::Party"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "partyRole",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents the role each specified party takes in the margin call. further to the principal roles, payer and receiver."
      } ],
      "type" : "model::external::cdm::PartyRole"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "clearingBroker",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Indicates the name of the Clearing Broker FCM/DCM."
      } ],
      "type" : "model::external::cdm::Party"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "callIdentifier",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents a unique Identifier for a margin call message, that can be referenced throughout all points of the process."
      } ],
      "type" : "model::external::cdm::Identifier"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "callAgreementType",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the legal agreement type the margin call is generated from and governed by."
      } ],
      "type" : "model::external::cdm::AgreementName"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "agreementMinimumTransferAmount",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the collateral legal agreement minimum transfer amount in base currency."
      } ],
      "type" : "model::external::cdm::Money"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "agreementThreshold",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the collateral legal agreement threshold amount in base currency."
      } ],
      "type" : "model::external::cdm::Money"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "agreementRounding",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Specifies the collateral legal agreement rounding in base currency."
      } ],
      "type" : "model::external::cdm::Money"
    }, {
      "multiplicity" : {
        "lowerBound" : 1,
        "upperBound" : 1
      },
      "name" : "regMarginType",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Identifies margin type and if related regulatory mandate"
      } ],
      "type" : "model::external::cdm::RegMarginTypeEnum"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "regIMRole",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Indicates the role of the party in an regulatory initial margin call instruction (i.e Pledgor party or Secured party)."
      } ],
      "type" : "model::external::cdm::RegIMRoleEnum"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "baseCurrencyExposure",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents the current mark to market value or IM calculation value of the trade portfolio as recorded by the principle (in base currency), to be referenced in a margin call."
      } ],
      "type" : "model::external::cdm::MarginCallExposure"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "collateralPortfolio",
      "stereotypes" : [ {
        "profile" : "model::external::cdm::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents attributes to define the details of collateral assets within a collateral portfolio to be used in margin call messaging and contribute to collateral balances e.g securities in a collateral account recorded by the principal as held or posted."
      } ],
      "type" : "model::external::cdm::CollateralPortfolio"
    }, {
      "multiplicity" : {
        "lowerBound" : 0,
        "upperBound" : 1
      },
      "name" : "independentAmountBalance",
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "Represents additional credit support amount over and above mark to market value."
      } ],
      "type" : "model::external::cdm::CollateralBalance"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "Represents common attributes required for Issuance and Response to a Margin Call action as a result of a demand for delivery or return of collateral determined under a legal agreement such as a credit support document or equivalent."
    } ]
  }
}